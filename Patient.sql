create table PATIENT(
    Patient_no int,
    Name varchar(255),
    DOB date,
    Address varchar(255),
    Prescription_no int,
    Drug varchar(255),
    Date date,
    Dosage varchar(255),
    Doctor varchar(255),
    Secretary varchar(255)
);