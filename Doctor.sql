create table DOCTOR(
Doctor_no int,
DoctorName varchar(255),
Secretary varchar(255),
Patient_no int,
PatientName varchar(255),
PatientDOB date,
PatientAddress varchar(255),
Prescription_no int,
Drug varchar(255),
Date date,
Dosage varchar(255)
);