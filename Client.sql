create table CLIENT(
    Client_No int,
    Name varchar(255),
    Location varchar(255),
    Manager_No int,
    Manager_name varchar(255),
    Manager_location varchar(255),
    Contract_No int,
    Estimated_cost varchar(255),
    Completion_date date,
    Staff_No int,
    Staff_name varchar(255),
    Staff_location varchar(255)
);