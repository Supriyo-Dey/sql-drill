create table BRANCH(
    Branch_no int,
    Branch_Addr varchar(255),
    ISBN varchar(17),
    Title varchar(255),
    Author varchar(255),
    Publisher varchar(255),
    Num_copies int
)